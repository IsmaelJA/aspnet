﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_Product.Models
{
    [MetadataType(typeof(Product.MetaData))]
    public partial class Product
    {
        sealed class MetaData
        {
            [Key]
            public int Prodid;

            [Required(ErrorMessage= "Ingresa el nombre del producto")]
            public string Prodname;

            [Required(ErrorMessage = "Ingresa una descripcion para el producto")]
            public string Descrip;

            [Required(ErrorMessage = "Ingresa el precio")]
            [Range(1, 500)]
            public Nullable<int> Price;

        }
    }
}