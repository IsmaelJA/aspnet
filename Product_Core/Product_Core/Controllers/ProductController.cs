﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Product_Core.Models;

namespace Product_Core.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _db; //Se define una variable para poder acceder a la base de datos

        public ProductController(ApplicationDbContext db) // Se define Constructor - Inyeccion de dependencias 
        {
            _db = db;
        }

        public IActionResult Index() //Metodo GET
        {
            var displaydata = _db.Product.ToList(); //Se define variable de tipo LISTA
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string empSearch) //Es un metodo sobrecargado 
        {
            ViewData["GetProdDetails"] = empSearch;
            var empquery = from x in _db.Product select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Prodname.Contains(empSearch)); //Retorna una lista
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        }

        // CREATE >>
        public IActionResult Create() //Este metodo se ejecuta cuando se precina Create
        {
            return View();
        }
        [HttpPost] //Este metodo se ejecuta cuando se genera la peticion de guardar al empleado
        public async Task<IActionResult> Create(Product nProd)
        {
            if(ModelState.IsValid) //Si el modelo es valido
            {
                _db.Add(nProd); //Agrega un nuevo producto
                await _db.SaveChangesAsync();
                return RedirectToAction("Index"); //Retorna a la vista Index con los valores nuevos
            }
            return View(nProd);
        }
        // << CREATE

        // READ >>
        public async Task<IActionResult> Detail (int? id) //Metodo para leer los datos del producto
        {
            if(id == null) //Si no existe el ID
            {
                return RedirectToAction("Index"); //Retorna a la vista Index
            }
            var getProdDetail = await _db.Product.FindAsync(id); //Se define una cariable para obtener los valores del producto seleccionado
            return View(getProdDetail);
        }
        // << READ

        // UPDATE >>
        public async Task<IActionResult> Edit(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var getProdDetail = await _db.Product.FindAsync(id);
            return View(getProdDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Product oldProd)
        {
            if(ModelState.IsValid)
            {
                _db.Update(oldProd);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldProd);
        }
        // << UPDATE
        
        // DELETE >>
        public async Task<IActionResult> Delete (int? id)
        {
            if(id==null)
            {
                return RedirectToAction("index");
            }
            var getProdDetail = await _db.Product.FindAsync(id);
            return View(getProdDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getProdDetail = await _db.Product.FindAsync(id);
            _db.Product.Remove(getProdDetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // << DELETE

    }
}
