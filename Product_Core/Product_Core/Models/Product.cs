﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Product_Core.Models
{
    public class Product
    {
        [Key]
        [Display(Name = "ID")]
        public int Prodid { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del producto")]
        [Display(Name = "Nombre del producto")]
        public String Prodname { get; set; }

        [Required(ErrorMessage = "Ingresa una descripcion")]
        [Display(Name = "Descripcion del producto")]
        public String Descrip { get; set; }

        [Required(ErrorMessage = "Ingresa el precio del producto")]
        [Display(Name = "Precio")]
        public int Price { get; set; }
    }
}
