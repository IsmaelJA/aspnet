CREATE DATABASE PROD;
USE PROD;

CREATE TABLE Product(
	Prodid int not null primary key identity(1,1),
	Prodname nvarchar(150),
	Descrip nvarchar(150),
	Price int,
);

Select * from Product;