﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Filters
{
    public class CustomExceptionHandler : FilterAttribute, IExceptionFilter //Se agrega al contexto global...
    {
        void IExceptionFilter.OnException(ExceptionContext filterContext)
        {
            var path = filterContext.HttpContext.Server.MapPath("/error/500.html"); //Para indicar la ruta a acceder en caso de error no controlado
            //Nota que la ruta está a nivel de solución, hay que crear la carpeta....
            var bytes = System.IO.File.ReadAllBytes(path);
            filterContext.Result = new FileContentResult(bytes, "text/html"); //Para mostrar la página
            filterContext.ExceptionHandled = true; //Indicamos que ya manejamos la excepcion
        }
    }
}