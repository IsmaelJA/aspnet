﻿using ProyectoMVCFramework1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product

        public ActionResult Index()
        {
            return View("Index"); //De esta manera puedo definir el nombre de la view dentro de la carpeta del nombre del controlador
            //o si es por fuera de la carpeta con algo tipo "~Views/Product/Nombre_de_la_vista.cshtml"
        }

        [Route("product/{productName}")]
        public ActionResult Detail(string productName)
        {
            ViewBag.Product = new Product {
                Name = "Women´s winter jacket",
                FullPrice = 20.00M,
                CurrentPrice = 16.00M,
                PercentOff = 20,
                ImagePath = "/Content/Images/Products/1.jpg",
                StarRating = 4
            }; //Agregamos un objeto tipo producto al ViewBag :D 
            return View();
        }
    }
}