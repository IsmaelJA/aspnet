﻿using Microsoft.Ajax.Utilities;
using ProyectoMVCFramework1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProyectoMVCFramework1.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost] //Porque el método del formulario es Post
        public ActionResult Index(Models.Login request) 
        {
            if (!ModelState.IsValid) return View(request);//Se activa si cualquier anotación del modelo no se cumple

            if (!string.IsNullOrEmpty(request.Username) && !string.IsNullOrEmpty(request.Password))
            {
                FormsAuthentication.SetAuthCookie(request.Username, false); //Seteamos la cookie de auth con username y false para que expire con la sesión
                return Redirect(FormsAuthentication.GetRedirectUrl(request.Username, false));
            }
            ViewBag.Failed = true; 
            return View(request); //Envía el modelo de regreso a la vista :D 
        }
    }
}