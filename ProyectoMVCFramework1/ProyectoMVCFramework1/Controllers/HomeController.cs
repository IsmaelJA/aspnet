﻿using ProyectoMVCFramework1.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Controllers
{
    [CrawlerFilter] //Agrega el filtro a este controlador
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //throw new System.Exception(); //Para probar el filtro de excepciones
            return View();
        }

    }
}