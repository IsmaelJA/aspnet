﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            //Es un ejemplo para aplicar redirección
            //Supongamos que el sitio en una versión anterior tenía un apartado PRODUCTS en vez de PRODUCT
            //Con este controlador resolvemos ese problema, redireccionando PRODUCTS a PRODUCT
            return RedirectToAction("Index","Product"); //Sirve para redireccionar al método de otro controlador
            //Es Método, Controlador
        }
    }
}