﻿using ProyectoMVCFramework1.Data;
using ProyectoMVCFramework1.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoMVCFramework1.Controllers
{
    [Authorize] //Es un filtro que pide al user estar autorizado para acceder a este contenido
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            Customer customer = null;
            using (var ctx = new HPlusSportDbContext()) {
                var user = ctx.Users.FirstOrDefault(u=>u.EmailAddress == User.Identity.Name);

                if (user != null)
                    customer = ctx.Customers.FirstOrDefault(c=>c.CustomerID == user.CustomerId);

                View();
            }

           return View(customer);
        }
    }
}