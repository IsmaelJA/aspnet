using System.Collections.Generic;

namespace ProyectoMVCFramework1.Data.Entities
{
    public partial class Cart
    {
        public Cart()
        {
            CartProducts = new HashSet<CartProduct>(); //Porque tiene una lista de productos en el carrito
        }

        public int CartId { get; set; }

        public int? CustomerId { get; set; }

        public virtual ICollection<CartProduct> CartProducts { get; set; } //Un carrito se puede asociar con muchos CartProducts por que es un many to many con Product

        public virtual Customer Customer { get; set; } //Un carrito solo puede tener un customer asociado
    }
}
