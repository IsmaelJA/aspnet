﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlString Copyright(this HtmlHelper helper)  //Indica que es un metodo de extensión de la clase HtmlHelper
        {
            return helper.Raw( $"&copy; H + Sport {DateTime.Now.Year}"); //Para codificarlo como html usamos helper.raw
        }
    }
}