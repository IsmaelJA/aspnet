using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ProyectoMVCFramework1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas(); //Es como organizar el proyecto en "paquetes" los busca automáticamente

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters); //Filtros de autenticación, autorizacion, exception, etc
            //Archivo en app_start/FilterConfig.cs  agrega filtros de manera global para todas las rutas

            RouteConfig.RegisterRoutes(RouteTable.Routes); //archivo app_start/routeconfig.cs

            BundleConfig.RegisterBundles(BundleTable.Bundles); //Registro de bundles en archivo app_start/BundleConfig.cs
            //Es una manera de combinar la descarga de archivos como CSS y JS para mejorar el rendimiento? D:
            //It seems like agregar cierta cantidad de JS bajo un mismo dominio por ejemplo
        }
    }
}
