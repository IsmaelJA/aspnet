﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


//Se implementa el patron DECORATOR, el cual permite agregar comportamientoa un objeto individual, sin afectar el comportamento de otros
// objetos de la misma clase
namespace REST_Emp.Models
{
    [MetadataType(typeof(Employee.MetaData))]
    public partial class Employee
    {
        sealed class MetaData //Esta clase no se puede instanciar 
        {
            [Key]
            public int Empid;

            [Required(ErrorMessage = "Ingresa el nombre del empleado")]
            public string Empname;

            [Required(ErrorMessage = "Ingresa el Email")]
            [EmailAddress(ErrorMessage = "Ingresa un Email valido")]
            public string Email;

            [Required(ErrorMessage = "Ingresa la edad")]
            [Range(20, 50)]
            public Nullable<int> Age;

            [Required(ErrorMessage = "Ingresa el salario")]
            public Nullable<int> Salary;

        }
    }
}