﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Empleados_Core.Models
{
    public class Employee
    {
        [Key]
        [Display(Name = "ID")]
        public int Empid { get; set; }

        [Required(ErrorMessage = "Ingresa el nombre del empleado")]
        [Display(Name = "Nombre del empleado")]
        public string Empname { get; set; }

        [Required(ErrorMessage = "Ingresa el Email")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Ingresa un Email valido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Ingresa la edad")]
        [Display(Name = "Edad")]
        [Range(20, 50)]
        public int Age { get; set; }

        [Required(ErrorMessage = "Ingresa el salario")]
        [Display(Name = "Salario")]
        public int Salary { get; set; }
    }
}
