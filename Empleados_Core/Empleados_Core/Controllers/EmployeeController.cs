﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Empleados_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.EntityFrameworkCore;

namespace Empleados_Core.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _db; // Variable para acceder a la base de datos

        public EmployeeController(ApplicationDbContext db) // Constructor - Inyeccion de dependencias 
        {
            _db = db;
        }

        public IActionResult Index()
        {
            //Se genera listado de los registros de la base de datos
            //Es un metodo GET
            var displaydata = _db.Employee.ToList(); //Variabe de tipo Lista
            return View(displaydata);
        }

        [HttpGet]
        public async Task<IActionResult> Index(string empSearch) //Es un metodo sobrecargado 
        {
            ViewData["GetEmployeeDetails"] = empSearch;
            var empquery = from x in _db.Employee select x;
            if (!String.IsNullOrEmpty(empSearch))
            {
                empquery = empquery.Where(x => x.Empname.Contains(empSearch) || //Retorna una lista
                x.Email.Contains(empSearch));
            }
            return View(await empquery.AsNoTracking().ToListAsync());
        } 

        //Metodo para crear (CREATE)
        public IActionResult Create() // Se ejecuta cuando se presiana Create New
        {
            return View();
        }
        [HttpPost] // Se ejecuta cuando se guarda el empleado
        public async Task<IActionResult> Create(Employee nEmp)
        {
            if(ModelState. IsValid)
            {
                _db.Add(nEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(nEmp);
        }

        //Metodo de detalle (READ)
        public async Task<IActionResult> Detail (int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }

        //Metodo actuaizar (UPDATE)
        public async Task<IActionResult> Edit (int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Employee oldEmp)
        {
            if(ModelState.IsValid)
            {
                _db.Update(oldEmp);
                await _db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldEmp);
        }

        //Metodo borrar (DELETE)
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getEmpDetail = await _db.Employee.FindAsync(id);
            return View(getEmpDetail);
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            var getEmpdetail = await _db.Employee.FindAsync(id);
            _db.Employee.Remove(getEmpdetail);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
